#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

int rollTheDice(int x = 6, int y = 3, int z = 0);

int main()
{
    int diceSides, diceNumber, diceModifyer;

    cout << "How many sides on the dice? (default 6) \t";
    cin >> diceSides;
    cout << endl << "How many times to roll the dice? (default 3) \t";
    cin >> diceNumber;
    cout << endl << "How to modify the final tally? (default 0) \t";
    cin >> diceModifyer;

    rollTheDice(diceSides, diceNumber, diceModifyer);

    return 0;
}
int rollTheDice(int x, int y, int z){

    srand(time(0));
    int diceTotal = 0, diceRoll = 0;

    for(int i = 1; i <= y; i++){
        diceRoll = rand()%x + 1;
        cout << diceRoll << "\t";
        diceTotal += diceRoll;
        }

    diceTotal += z;
    cout << "modifier " << z << "\t Total " << diceTotal << endl;

    return 0;

}
