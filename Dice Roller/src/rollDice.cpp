#include <iostream>
#include <ctime>
#include <cstdlib>
#include "rollDice.h"

using namespace std;

rollDice::rollDice(int a, int b, int c)
:sides(a), roll(b), mod(c)
{
}

int rollDice::resultDice(){

    srand(time(0));

    for(int i = 1; i <= roll; i++){
        diceRoll = rand()%sides + 1;
        cout << diceRoll << "\t";
        diceTotal += diceRoll;
        }

    diceTotal += mod;
    cout << "modifier " << mod << "\t Total " << diceTotal << endl;

    return 0;
}
