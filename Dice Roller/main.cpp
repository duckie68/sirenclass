#include <iostream>
#include <ctime>
#include <cstdlib>
#include "rollDice.h"

using namespace std;

int main()
{
    int diceSides, diceNumber, diceModifyer;

    cout << "How many sides on the dice? \t";
    cin >> diceSides;
    cout << endl << "How many times to roll the dice? \t";
    cin >> diceNumber;
    cout << endl << "How to modify the final tally? \t";
    cin >> diceModifyer;

    rollDice roll(diceSides, diceNumber, diceModifyer);
    roll.resultDice();

    return 0;
}
