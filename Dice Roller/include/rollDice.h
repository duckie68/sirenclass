#ifndef ROLLDICE_H
#define ROLLDICE_H


class rollDice
{
    public:
        rollDice(int a, int b, int c);
        int resultDice();
    private:
        int sides;
        int roll;
        int mod;
        int diceTotal = 0;
        int diceRoll = 0;
};

#endif // ROLLDICE_H
