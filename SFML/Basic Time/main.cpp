#include <SFML/Graphics.hpp>
#include<iostream>

int main()
{
    // Create the main window
    sf::RenderWindow Window(sf::VideoMode(800, 600), "Basic Time Demo");

    //Clock object
    sf::Clock Clock;

    // Call time object, set units as seconds, store 2 seconds in it.  Alternatives are micro and milliseconds.
    // Display time can be called asSeconds, etc.
    sf::Time Time = sf::seconds(2);

    // Display example
    std::cout << Time.asSeconds() << std::endl;

	// Start the game loop
    while (Window.isOpen())
    {
        // Process events
        sf::Event event;
        while (Window.pollEvent(event))
        {
            // Close window : exit
            if (event.type == sf::Event::Closed)
                Window.close();
        }

        // Time becomes equal to time elapsed since clock object created
        // or since clock restart.  Updates every cycle.
        Time = Clock.getElapsedTime();
        std::cout << Time.asSeconds() << std::endl;
        // This could be shortened by removing the restart and changing the above to
        // Time = Clock.restart();
        Clock.restart();



        // Update the window
        Window.display();
    }

    return EXIT_SUCCESS;
}
