# README #

This is the practice repository for Siren Productions Media.

### What is this repository for? ###

We do special effects for film, and now our work is branching out to where it would be useful as hell to be able to program.  Special effects is different from visual effects in that we work with things rather than digital constructs, and lately we have been using lots of analog electronics, discrete logic, and microcontrollers.

When running face first into the microcontrollers, we realized that using a real programming language would be useful as hell, so my colleagues and I are dusting off some very dusty old skills in an attempt to relearn some basic programming, as well as branch out into some advanced programming.  This particular repo will be our practice area.

The main goal of this repo is just to start out small.  The skill levels of the participants is a big mish mash of advanced knowledge and lack of practice, to beginner knowledge in need of simple working examples.  Small bits of code simply seems more reasonable as far as getting started is concerned.

The code that we make ourselves in this repo is under no license restrictions, you may use it as you wish, but beware; it is from very basic programmers and may not be on top of 'best practices' in coding.  Any code we fork into here will follow the original authors license.  Any code we use for actual release purposes will be packaged with it's own license in a separate repo.

Feel free to offer suggestions or chat us up.  We'll also try to assist you if you are a beginner who can use some basic examples, but your mileage may vary.  Don't expect to get any awesome grades if you are using this work to supplement classwork.  If I throw you off my mis-speaking and calling classes functions or whatever, it's only because I'll know what I mean, I just don't say it right ;)

### How do I get set up? ###

I'm using Eclipse as an IDE for all this.  It's cross platform, supports pretty much every language, and it can work with git.  I used this link to help me set up the support for git; http://crunchify.com/how-to-configure-bitbucket-git-repository-in-you-eclipse/ .

(Changed 02 February 2015)
I am already discovering that I do not like Eclipse at all.  What a damn shame.  Back to CodeBlocks, and I'm using git-gui to interface with Bitbucket here.'

### Contribution guidelines ###

Not a whole lot to do as far as contributing, but if you set up a similar system for similar reasons, get in touch and we can review one anothers work.

### Who do I talk to? ###

D. Duckie Rodriguez (owner)
